# Glarch Package Repository

## How to Use

Add below text to '/etc/pacman.conf'.

```text
[glarch]
SigLevel = Never
Server = https://gitlab.com/glarch/repository/glarch/raw/master/x86_64/
```

Run 'pacman -Syy'

## Compiler and Linker Flags

```bash
CPPFLAGS="-D_FORTIFY_SOURCE=2"
CFLAGS="-march=x86-64 -mtune=generic -O2 -pipe -fstack-protector-strong -fno-plt"
CXXFLAGS="-march=x86-64 -mtune=generic -O2 -pipe -fstack-protector-strong -fno-plt"
LDFLAGS="-Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now"
```
